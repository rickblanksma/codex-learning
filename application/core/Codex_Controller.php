<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Codex_Controller extends CI_Controller {
	protected $data = Array();
	
	function __construct()
	{
		parent::__construct();
	}

	protected function _render($view)
	{
		$toTpl["title"] = 'codex';


		$toBody["content_body"] = $this->load->view($view,array_merge($this->data,$toTpl),true);

		$this->load->view("template/labs/skeleton",$toBody,false);
	}
}